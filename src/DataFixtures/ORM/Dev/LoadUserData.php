<?php

namespace App\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @var EncoderFactory
     */
    private $encoderFactory;

    public function __construct(EncoderFactory $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $users = array(
            'baptiste' => 'pwd'
        );

        foreach ($users as $name => $password) {
            $user = new User();
            $user->setUsername($name);
            $user->setEmail($name . '@example.com');
            $user->setEnabled(TRUE);
            $encodedPw = $this->encoderFactory
                ->getEncoder($user)
                ->encodePassword($password, $user->getSalt())
            ;
            $user->setPassword($encodedPw);
            $this->addReference($name, $user);

            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4;
    }
}