<?php

namespace App;

use Gedmo\Loggable\LoggableListener;
use Gedmo\Translatable\TranslatableListener;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Translation\TranslatorInterface;

class DoctrineExtensionListener
{
    /**
     * @var TranslatableListener
     */
    protected $gedmoTranslatableListener;

    /**
     * @var LoggableListener
     */
    protected $gedmoLoggableListener;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @var AuthorizationChecker
     */
    protected $authorizationChecker;

    public function setGedmoTranslatableListener(TranslatableListener $listener = null)
    {
        $this->gedmoTranslatableListener = $listener;
    }

    public function setGedmoLoggableListener(LoggableListener $listener)
    {
        $this->gedmoLoggableListener = $listener;
    }

    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function setTokenStorage(TokenStorage $storage)
    {
        $this->tokenStorage = $storage;
    }

    public function setAuthorizationChecker(AuthorizationChecker $checker)
    {
        $this->authorizationChecker = $checker;
    }

    public function onLateKernelRequest(GetResponseEvent $event)
    {
        $this
            ->gedmoTranslatableListener
            ->setTranslatableLocale($event->getRequest()->getLocale())
        ;
    }

    public function onConsoleCommand()
    {
        $this
            ->gedmoTranslatableListener
            ->setTranslatableLocale($this->translator->getLocale())
        ;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $token = $this->tokenStorage->getToken();
        if (null !== $token && $this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->gedmoLoggableListener->setUsername($token->getUser());
        }
    }
}