<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Word
 *
 * @ORM\Table(name="ovski_word", uniqueConstraints={
 *     @ORM\UniqueConstraint(
 *         name="unique_word_idx",
 *         columns={"article_id", "word_type_id", "language_id", "value"}
 *     )
 * })
 * @ORM\Entity
 */
class Word
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    protected $value;

    /**
     * @var Article
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Article")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $article;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\WordType")
     * @ORM\JoinColumn(nullable=true, name="word_type_id")
     */
    protected $wordType;

    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Language")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $language;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="words")
     * @ORM\JoinTable(name="ovski_user_word")
     */
    protected $users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * Word to string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Word
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set article
     *
     * @param string $article
     * @return Word
     */
    public function setArticle($article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return string 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set language
     *
     * @param \App\Entity\Language $language
     * @return Word
     */
    public function setLanguage(\App\Entity\Language $language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \App\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set wordType
     *
     * @param \App\Entity\WordType $wordType
     * @return Word
     */
    public function setWordType(\App\Entity\WordType $wordType)
    {
        $this->wordType = $wordType;

        return $this;
    }

    /**
     * Get wordType
     *
     * @return \App\Entity\WordType 
     */
    public function getWordType()
    {
        return $this->wordType;
    }

    /**
     * Add user
     *
     * @param \App\Entity\User $user
     * @return Word
     */
    public function addUser(\App\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \App\Entity\User $user
     */
    public function removeUser(\App\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }
}
