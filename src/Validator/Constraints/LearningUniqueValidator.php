<?php

namespace App\Validator\Constraints;

use App\Entity\Learning;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LearningUniqueValidator extends ConstraintValidator
{
    private $em;
    private $tokenStorage;

    /**
     * Constructor
     *
     * @param EntityManager $em
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(EntityManager $em, TokenStorageInterface $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public function validate($learning, Constraint $constraint)
    {
        $userId = $this->tokenStorage->getToken()->getUser()->getId();
        $language1Id = $learning->getLanguage1()->getId();
        $language2Id = $learning->getLanguage2()->getId();

        $learning1 = $this->em->getRepository(Learning::class)->getByUser(
            $userId,
            array(
                'language1' => $language1Id,
                'language2' => $language2Id
            )
        );
        if ($learning1) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }

        $learning2 = $this->em->getRepository(Learning::class)->getByUser(
            $this->tokenStorage->getToken()->getUser()->getId(),
            array(
                'language1' => $language2Id,
                'language2' => $language1Id
            )
        );
        if ($learning2) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}