<?php

namespace App\Validator\Constraints;

use App\Entity\WordType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TranslationRequiresArticlesValidator extends ConstraintValidator
{
    private $em;

    /**
     * Constructor
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function validate($translation, Constraint $constraint)
    {
        // nothing to check if no word type was given
        if (!$translation->getWordType()) {
            return;
        }

        $nameArticle = $this->em->getRepository(WordType::class)->getDefaultWordTypeValueById(
            $translation->getWordType()->getId()
        );

        // if the words have name as wordType,
        // they must have an article except if the language has requiredArticle to false
        if ($nameArticle == 'name') {
            if (!$translation->getWord1()->getArticle() && $translation->getLearning()->getLanguage1()->requireArticles())
            {
                $this->context->buildViolation($constraint->message)
                    ->atPath('word1')
                    ->setParameter('%language%', $translation->getLearning()->getLanguage1()->getName())
                    ->addViolation()
                ;
            }

            if (!$translation->getWord2()->getArticle() && $translation->getLearning()->getLanguage2()->requireArticles())
            {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('%language%', $translation->getLearning()->getLanguage2()->getName())
                    ->addViolation()
                ;
            }
        }
    }
}