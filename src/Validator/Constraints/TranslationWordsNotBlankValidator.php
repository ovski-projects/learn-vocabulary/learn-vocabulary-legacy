<?php

namespace App\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Security\Core\SecurityContext;

class TranslationWordsNotBlankValidator extends ConstraintValidator
{
    public function validate($translation, Constraint $constraint)
    {
        $word1Value = $translation->getWord1()->getValue();
        if (empty($word1Value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%language%', $translation->getLearning()->getLanguage1()->getName())
                ->addViolation()
            ;
        }
        $word2Value = $translation->getWord2()->getValue();
        if (empty($word2Value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%language%', $translation->getLearning()->getLanguage2()->getName())
                ->addViolation()
            ;
        }
    }
}