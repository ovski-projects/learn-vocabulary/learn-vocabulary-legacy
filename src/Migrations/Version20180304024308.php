<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180304024308 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ext_translations (id INT AUTO_INCREMENT NOT NULL, locale VARCHAR(8) NOT NULL, object_class VARCHAR(255) NOT NULL, field VARCHAR(32) NOT NULL, foreign_key VARCHAR(64) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX translations_lookup_idx (locale, object_class, foreign_key), UNIQUE INDEX lookup_unique_idx (locale, object_class, field, foreign_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ovski_translation (id INT AUTO_INCREMENT NOT NULL, word1_id INT NOT NULL, word2_id INT NOT NULL, word_type_id INT DEFAULT NULL, learning_id INT NOT NULL, user_id INT NOT NULL, created_at DATETIME NOT NULL, is_starred TINYINT(1) NOT NULL, INDEX IDX_3CDCAAC24586854D (word1_id), INDEX IDX_3CDCAAC257332AA3 (word2_id), INDEX IDX_3CDCAAC2CF3EFB60 (word_type_id), INDEX IDX_3CDCAAC24E6B0AB3 (learning_id), INDEX IDX_3CDCAAC2A76ED395 (user_id), UNIQUE INDEX unique_translation_idx (word1_id, word2_id, learning_id, user_id, word_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ovski_learning (id INT AUTO_INCREMENT NOT NULL, language1_id INT NOT NULL, language2_id INT NOT NULL, slug VARCHAR(255) NOT NULL, INDEX IDX_1BFC798F6C3EEA2C (language1_id), INDEX IDX_1BFC798F7E8B45C2 (language2_id), UNIQUE INDEX unique_learning_idx (language1_id, language2_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ovski_user_learning (learning_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_494F53A4E6B0AB3 (learning_id), INDEX IDX_494F53AA76ED395 (user_id), PRIMARY KEY(learning_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ovski_word_type (id INT AUTO_INCREMENT NOT NULL, value VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ovski_language (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, require_articles TINYINT(1) NOT NULL, UNIQUE INDEX unique_language_idx (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ovski_article (id INT AUTO_INCREMENT NOT NULL, language_id INT NOT NULL, value VARCHAR(255) NOT NULL, INDEX IDX_40D7A10582F1BAF4 (language_id), UNIQUE INDEX unique_article_idx (value, language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ovski_word (id INT AUTO_INCREMENT NOT NULL, article_id INT DEFAULT NULL, word_type_id INT DEFAULT NULL, language_id INT NOT NULL, value VARCHAR(255) NOT NULL, INDEX IDX_9CBCAF3D7294869C (article_id), INDEX IDX_9CBCAF3DCF3EFB60 (word_type_id), INDEX IDX_9CBCAF3D82F1BAF4 (language_id), UNIQUE INDEX unique_word_idx (article_id, word_type_id, language_id, value), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ovski_user_word (word_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_B1C9FD28E357438D (word_id), INDEX IDX_B1C9FD28A76ED395 (user_id), PRIMARY KEY(word_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ovski_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', max_items_per_page INT NOT NULL, UNIQUE INDEX UNIQ_D2DE0C6592FC23A8 (username_canonical), UNIQUE INDEX UNIQ_D2DE0C65A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_D2DE0C65C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ovski_translation ADD CONSTRAINT FK_3CDCAAC24586854D FOREIGN KEY (word1_id) REFERENCES ovski_word (id)');
        $this->addSql('ALTER TABLE ovski_translation ADD CONSTRAINT FK_3CDCAAC257332AA3 FOREIGN KEY (word2_id) REFERENCES ovski_word (id)');
        $this->addSql('ALTER TABLE ovski_translation ADD CONSTRAINT FK_3CDCAAC2CF3EFB60 FOREIGN KEY (word_type_id) REFERENCES ovski_word_type (id)');
        $this->addSql('ALTER TABLE ovski_translation ADD CONSTRAINT FK_3CDCAAC24E6B0AB3 FOREIGN KEY (learning_id) REFERENCES ovski_learning (id)');
        $this->addSql('ALTER TABLE ovski_translation ADD CONSTRAINT FK_3CDCAAC2A76ED395 FOREIGN KEY (user_id) REFERENCES ovski_user (id)');
        $this->addSql('ALTER TABLE ovski_learning ADD CONSTRAINT FK_1BFC798F6C3EEA2C FOREIGN KEY (language1_id) REFERENCES ovski_language (id)');
        $this->addSql('ALTER TABLE ovski_learning ADD CONSTRAINT FK_1BFC798F7E8B45C2 FOREIGN KEY (language2_id) REFERENCES ovski_language (id)');
        $this->addSql('ALTER TABLE ovski_user_learning ADD CONSTRAINT FK_494F53A4E6B0AB3 FOREIGN KEY (learning_id) REFERENCES ovski_learning (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ovski_user_learning ADD CONSTRAINT FK_494F53AA76ED395 FOREIGN KEY (user_id) REFERENCES ovski_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ovski_article ADD CONSTRAINT FK_40D7A10582F1BAF4 FOREIGN KEY (language_id) REFERENCES ovski_language (id)');
        $this->addSql('ALTER TABLE ovski_word ADD CONSTRAINT FK_9CBCAF3D7294869C FOREIGN KEY (article_id) REFERENCES ovski_article (id)');
        $this->addSql('ALTER TABLE ovski_word ADD CONSTRAINT FK_9CBCAF3DCF3EFB60 FOREIGN KEY (word_type_id) REFERENCES ovski_word_type (id)');
        $this->addSql('ALTER TABLE ovski_word ADD CONSTRAINT FK_9CBCAF3D82F1BAF4 FOREIGN KEY (language_id) REFERENCES ovski_language (id)');
        $this->addSql('ALTER TABLE ovski_user_word ADD CONSTRAINT FK_B1C9FD28E357438D FOREIGN KEY (word_id) REFERENCES ovski_word (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ovski_user_word ADD CONSTRAINT FK_B1C9FD28A76ED395 FOREIGN KEY (user_id) REFERENCES ovski_user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ovski_translation DROP FOREIGN KEY FK_3CDCAAC24E6B0AB3');
        $this->addSql('ALTER TABLE ovski_user_learning DROP FOREIGN KEY FK_494F53A4E6B0AB3');
        $this->addSql('ALTER TABLE ovski_translation DROP FOREIGN KEY FK_3CDCAAC2CF3EFB60');
        $this->addSql('ALTER TABLE ovski_word DROP FOREIGN KEY FK_9CBCAF3DCF3EFB60');
        $this->addSql('ALTER TABLE ovski_learning DROP FOREIGN KEY FK_1BFC798F6C3EEA2C');
        $this->addSql('ALTER TABLE ovski_learning DROP FOREIGN KEY FK_1BFC798F7E8B45C2');
        $this->addSql('ALTER TABLE ovski_article DROP FOREIGN KEY FK_40D7A10582F1BAF4');
        $this->addSql('ALTER TABLE ovski_word DROP FOREIGN KEY FK_9CBCAF3D82F1BAF4');
        $this->addSql('ALTER TABLE ovski_word DROP FOREIGN KEY FK_9CBCAF3D7294869C');
        $this->addSql('ALTER TABLE ovski_translation DROP FOREIGN KEY FK_3CDCAAC24586854D');
        $this->addSql('ALTER TABLE ovski_translation DROP FOREIGN KEY FK_3CDCAAC257332AA3');
        $this->addSql('ALTER TABLE ovski_user_word DROP FOREIGN KEY FK_B1C9FD28E357438D');
        $this->addSql('ALTER TABLE ovski_translation DROP FOREIGN KEY FK_3CDCAAC2A76ED395');
        $this->addSql('ALTER TABLE ovski_user_learning DROP FOREIGN KEY FK_494F53AA76ED395');
        $this->addSql('ALTER TABLE ovski_user_word DROP FOREIGN KEY FK_B1C9FD28A76ED395');
        $this->addSql('DROP TABLE ext_translations');
        $this->addSql('DROP TABLE ovski_translation');
        $this->addSql('DROP TABLE ovski_learning');
        $this->addSql('DROP TABLE ovski_user_learning');
        $this->addSql('DROP TABLE ovski_word_type');
        $this->addSql('DROP TABLE ovski_language');
        $this->addSql('DROP TABLE ovski_article');
        $this->addSql('DROP TABLE ovski_word');
        $this->addSql('DROP TABLE ovski_user_word');
        $this->addSql('DROP TABLE ovski_user');
    }
}
