<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;

class ApplicationAvailabilityFunctionalTest extends BaseFunctionalTest
{
    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    /**
     * @dataProvider authenticatedUrlProvider
     */
    public function testAuthPageIsSuccessful($url)
    {
        $client = $this->logIn();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function urlProvider()
    {
        $urlsWithLocal = array(
            '/',
            '/login',
            '/old-school',
            '/privacy-policy',
            '/tips',
            '/contact',
            '/register/',
            '/resetting/request'
        );

        $urls = array(
            array('/'),
        );

        // Prefix the urls with a local by locals
        foreach ($urlsWithLocal as $urlWithLocal) {
            $urls[] = array('/fr' . $urlWithLocal);
            $urls[] = array('/en' . $urlWithLocal);
        }

        return $urls;

    }

    public function authenticatedUrlProvider()
    {
        $urlsWithLocal = array(
            '/languages/',
            '/languages/new',
            '/revision/french-spanish',
            '/edition/french-spanish',
            '/edition/french-spanish/1/edit',
            '/settings/',
            '/profile/',
            '/profile/edit',
            '/profile/change-password',
        );

        $urls = array();

        // Prefix the urls with a local by locals
        foreach ($urlsWithLocal as $urlWithLocal) {
            $urls[] = array('/fr' . $urlWithLocal);
            $urls[] = array('/en' . $urlWithLocal);
        }

        return $urls;
    }
}
