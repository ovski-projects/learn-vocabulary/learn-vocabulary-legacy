<?php

namespace App\Tests;

class FormsFunctionalTest extends BaseFunctionalTest
{
    public function testLearningIsCreated()
    {
        $client = $this->logIn();
        $crawler = $client->request('GET', '/fr/languages/new');

        $form = $crawler->selectButton('Valider')->form();
        $form['ovski_languagebundle_learning[language1]'] = '1';
        $form['ovski_languagebundle_learning[language2]'] = '3';

        $crawler = $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirection());
    }

    public function testLearningIsValidated()
    {
        $client = $this->logIn();
        $crawler = $client->request('GET', '/fr/languages/new');

        $form = $crawler->selectButton('Valider')->form();
        $form['ovski_languagebundle_learning[language1]'] = '1';
        $form['ovski_languagebundle_learning[language2]'] = '1';

        $crawler = $client->submit($form);

        $this->assertTrue($client->getResponse()->isOk());
    }

    public function testTranslationIsCreated()
    {
        $client = $this->logIn();
        $crawler = $client->request('GET', '/en/edition/french-spanish');

        $form = $crawler->selectButton('Add')->form();
        $form['ovski_languagebundle_translation[word1][value]'] = 'el hombre';
        $form['ovski_languagebundle_translation[word2][value]'] = 'l\'homme';

        $crawler = $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirection());

        $crawler = $client->followRedirect();

        $this->assertTrue($crawler->filter('html:contains("hombre")')->count() == 1);
    }
}
