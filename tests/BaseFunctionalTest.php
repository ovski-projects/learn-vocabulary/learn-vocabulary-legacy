<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;

Abstract class BaseFunctionalTest extends WebTestCase
{
    /**
     * @var Application
     */
    public static $application;

    /**
     * {@inheritDoc}
     */
    public static function setUpBeforeClass()
    {
        self::bootKernel();

        self::$application = new Application(self::$kernel);
        self::$application->setAutoExit(false);

        self::runCommand('doctrine:database:create');
        self::runCommand('doctrine:schema:update --force'); // doctrine migrations fail on sqlite
        self::runCommand('doctrine:fixtures:load --append');
    }

    /**
     * {@inheritDoc}
     */
    public static function tearDownAfterClass()
    {
        self::runCommand('doctrine:database:drop --force');
    }

    /**
     * Log a user in
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    protected function logIn()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/en/login');
        $buttonCrawlerNode = $crawler->selectButton('Log in');
        $form = $buttonCrawlerNode->form();

        $data = array(
            '_username' => 'baptiste',
            '_password' => 'pwd'
        );

        $client->submit($form, $data);
        $client->followRedirect();

        return $client;
    }

    /**
     * Run a command
     *
     * @param $command
     */
    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);
        self::$application->run(new StringInput($command));
    }
}