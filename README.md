Learn Vocabulary
=================

http://learn-vocabulary.com

An app created to learn symfony (initial commit in 2014) as well as english and spanish vocabulary.

Installation
------------

```
docker network create learn-vocabulary
docker-compose -f .docker/proxy-docker-compose.yml up -d
docker-compose up -d
mkdir var
make composer-install
make pbc cmd="doctrine:migrations:migrate"
make pbc cmd="doctrine:fixtures:load --append"
```

Update your `/etc/hosts` file by adding this line:

```
127.0.0.1       learn_vocabulary.ovski.docker adminer.learn_vocabulary.ovski.docker
```

You can browse http://learn_vocabulary.ovski.docker/

Use the debugger
----------------

Update .vscode php configuration

```
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Listen for XDebug",
            "type": "php",
            "request": "launch",
            "port": 9000,
            "localSourceRoot": "${workspaceRoot}",
            "serverSourceRoot": "/var/www/html"
        }
    ]
}
```

Run the tests
-------------

```bash
docker-compose run --rm php php ./vendor/symfony/phpunit-bridge/bin/simple-phpunit
```
