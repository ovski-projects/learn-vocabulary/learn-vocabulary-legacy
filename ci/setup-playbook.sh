#!/bin/bash

mkdir -p /root/.ssh/
eval $(ssh-agent -s)

# Add ssh keys to run the playbook on a remote server
echo "$RATCHET_SSH_PRIVATE_KEY" > /root/.ssh/id_rsa_ratchet
chmod 400 /root/.ssh/id_rsa_ratchet

# Add ssh keys to clone the playbooks repository
echo "$GITLAB_SSH_PRIVATE_KEY" > /root/.ssh/id_rsa_gitlab
chmod 400 /root/.ssh/id_rsa_gitlab
ssh-add /root/.ssh/id_rsa_gitlab
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > /root/.ssh/config

# clone the playbook and setup the hosts file
git clone git@gitlab.com:ovski-projects/ansible/playbooks.git /var/playbooks
mkdir -p /etc/ansible
mv /var/playbooks/hosts /etc/ansible/hosts
